const express = require('express');
const logger = require('./logger');

const app = express();

app.use(logger);
app.use('/kanjiConverter', require('./routes/api/kanji_converter'));

const PORT = process.env.PORT || 5000

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));