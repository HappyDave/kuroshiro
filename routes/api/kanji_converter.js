const express = require('express');
const asyncHandler = require('../../asyncHandler');
const Kuroshiro = require('kuroshiro');
const KuromojiAnalyzer = require('kuroshiro-analyzer-kuromoji');

const router = express.Router();
const kuroshiro = new Kuroshiro();

(async () => await kuroshiro.init(new KuromojiAnalyzer()))()

router.get('/', asyncHandler(async (req, res, next) => {
    if (!req.query.text) {
        res.status(400).json({ message: 'text param is required.' });
    }
    const result = await kuroshiro.convert(req.query.text,
        { to: "hiragana" });

    res.json({ message: result });
}));

module.exports = router;