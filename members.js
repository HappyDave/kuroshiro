const members = [
    {
        id: 1,
        name: 'Jonas',
        email: 'jonas@somefakemail.com',
        status: 'active'
    },
    {
        id: 2,
        name: 'Cris',
        email: 'cris@somefakemail.com',
        status : 'active'
    },
    {
        id : 3,
        name : 'Mickel',
        email : 'mickel@somefakemail.com',
        status : 'active'
    }
];

module.exports = members;