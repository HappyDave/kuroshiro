module.exports = {
    apps: [
        {
            name: "kanji_converter",
            script: "./index.js",
            env: {
                PORT: 5000,
                NODE_ENV: "production"
            }
        }
    ],
    deploy: {
        production: {
            user: "ubuntu",
            host: "ec2-18-218-166-65.us-east-2.compute.amazonaws.com",
            key: "E:/Documents/ssh_keys/aws_server.pem",
            ref: "origin/development",
            repo: "git@bitbucket.org:HappyDave/kuroshiro.git",
            path: "/home/ubuntu/production",
            "post-deploy":
                "npm install && pm2 startOrRestart ecosystem.config.js"
        }
    }
};